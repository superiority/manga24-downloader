"use strict";

let request = require("request");
let fs = require("fs");
let PDFImagePack = require("pdf-image-pack");

let format = require("util").format;
let mangaName = process.argv[2];
let downloadUrl = "http://img4.manga24.ru/manga/%s/%s/%s.%s";

let images = [];
let output  = mangaName + ".pdf";

let imageType = {
    jpeg:"jpeg",
    png: "png"
};

function downloadImage(path, url) {
    return new Promise((resolve, reject)=>{
        let req = request.get(url);
        req.on('response', function(response) {
            if(response.statusCode == 200)
                req.pipe(fs.createWriteStream(path)).on("close", resolve);
            else
                reject({code:response.statusCode});
        });
    });
}

function formatNumber (number){
    number = number.toString();
    if (number.length == 1)
        number = "00" + number;
    if (number.length == 2)
        number = "0" + number;
    return number;
}

function isAlreadyDownloaded(first, second){
    return new Promise((resolve, reject)=>{
        let basePath = mangaName + "/" + first + second + ".";
        let path = basePath + imageType.jpeg;
        fs.stat(path,(err, stat)=>{
            if (err) {
                let path = basePath + imageType.png;
                fs.stat(path, (err, stat)=> {
                    if (err) {
                        reject();
                    } else {
                        images.push(path);
                        resolve();
                    }
                });
            } else {
                images.push(path);
                resolve();
            }
        });
    });
}

function getPage(first, second) {
    return new Promise((resolve, reject)=> {
        isAlreadyDownloaded(first, second)
            .then(resolve)
            .catch(()=> {
                let path = mangaName + "/" + first + second + "." + imageType.jpeg;
                let url = format(downloadUrl, mangaName, first, second, imageType.jpeg);

                downloadImage(path, url)
                    .then(()=> {
                        images.push(path);
                        resolve();
                    })
                    .catch((e)=> {
                        let path = mangaName + "/" + first + second + "." + imageType.png;
                        let url = format(downloadUrl, mangaName, first, second, imageType.png);
                        downloadImage(path, url)
                            .then(()=> {
                                images.push(path);
                                resolve();
                            })
                            .catch((e)=> {
                                e.code != 403 && console.error(e);
                                reject();
                            })
                    })
            });
    })
}

function downloadAll(){
    let firstNumber = 1;
    let secondNumber = 1;
    let lastChanged = "second";

    return (function download() {
        let first = formatNumber(firstNumber);
        let second = formatNumber(secondNumber);

        console.log(first + "-" + second);

        return getPage(first, second)
            .then(()=> {
                secondNumber++;
                lastChanged = "second";
                return download();
            })
            .catch((e)=> {
                e && console.error(e);
                if (lastChanged == "second") {
                    firstNumber++;
                    secondNumber = 1;
                    lastChanged = "first";
                    return download();
                }
                else
                    return "Downloaded";
            });
    })();
}

fs.mkdir(mangaName, function(e){
    if(!e || (e && e.code === 'EEXIST')){
        downloadAll()
            .then(()=>{
                var slide = new PDFImagePack();
                slide.output(images, output, function(err, doc){
                    err && console.error(err);
                    console.log("Pdf generated.")
                })
            })
            .catch(console.error);
    } else {
        console.error(e);
    }
});